package com.services.demofeign.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("uri")
public class Demo {
	
	@RequestMapping("data")
	public String getData() {
		return   "JSON  !!!";
	}

}
